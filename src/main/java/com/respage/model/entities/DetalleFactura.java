package com.respage.model.entities;

import java.io.Serializable;



public class DetalleFactura implements Serializable{
	
	private int id;
	private int factura;
	private int cocinero;
	private String plato;
	private int importe;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getFactura() {
		return factura;
	}
	public void setFactura(int factura) {
		this.factura = factura;
	}
	public int getCocinero() {
		return cocinero;
	}
	public void setCocinero(int cocinero) {
		this.cocinero = cocinero;
	}
	public String getPlato() {
		return plato;
	}
	public void setPlato(String plato) {
		this.plato = plato;
	}
	public int getImporte() {
		return importe;
	}
	public void setImporte(int importe) {
		this.importe = importe;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + cocinero;
		result = prime * result + factura;
		result = prime * result + id;
		result = prime * result + importe;
		result = prime * result + ((plato == null) ? 0 : plato.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DetalleFactura other = (DetalleFactura) obj;
		if (cocinero != other.cocinero)
			return false;
		if (factura != other.factura)
			return false;
		if (id != other.id)
			return false;
		if (importe != other.importe)
			return false;
		if (plato == null) {
			if (other.plato != null)
				return false;
		} else if (!plato.equals(other.plato))
			return false;
		return true;
	}
	

	
	
}
