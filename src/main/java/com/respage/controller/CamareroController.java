package com.respage.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.UriComponentsBuilder;

import com.respage.model.entities.Camarero;
import com.respage.model.services.CamareroService;

@Controller
@RequestMapping("camarero")
public class CamareroController {
	@Autowired
	CamareroService camareroService; 
	@GetMapping("camarero/{id}")
	public ResponseEntity<Camarero> getCamareroId(@PathVariable("id") Integer id){
		Camarero camarero = camareroService.findOne(id);
		return new ResponseEntity<Camarero> (camarero, HttpStatus.OK);
	}
	@GetMapping("camarero")
	public ResponseEntity<List<Camarero>> getAllCamarero(){
		final List<Camarero> listcamarero = new ArrayList<Camarero>();
		camareroService.findAll().forEach(new Consumer<Camarero>() {
			@Override
			public void accept(Camarero e) {
				listcamarero.add(e);
			}
		});
		return new ResponseEntity<List<Camarero>>(listcamarero,HttpStatus.OK);
	}
	@PostMapping("camarero")
	public ResponseEntity<Camarero> addCamarero(@RequestBody Camarero camarero, UriComponentsBuilder builder){
		Camarero flag = camareroService.create(camarero);
		if(flag==null) {
			return new ResponseEntity<Camarero>(flag,HttpStatus.CONFLICT);	
		}
		return new ResponseEntity<Camarero>(flag,HttpStatus.OK);
	}
	
	@PutMapping("camarero/{id}")
	public ResponseEntity<Camarero> updateCamarero(@RequestBody Camarero camarero,@PathVariable("id") Integer id){
		Camarero flag = camareroService.update(id, camarero);
		if(flag==null) {
			return new ResponseEntity<Camarero>(flag,HttpStatus.CONFLICT);	
		}
		return new ResponseEntity<Camarero>(flag,HttpStatus.OK);
	}
	@DeleteMapping("camarero/{id}")
	public ResponseEntity<Void> deleteCamarero(@PathVariable("id") Integer id){
		camareroService.delete(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
}