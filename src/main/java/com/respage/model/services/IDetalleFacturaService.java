package com.respage.model.services;

import java.util.List;

import com.respage.model.entities.DetalleFactura;

public interface IDetalleFacturaService  {

	public DetalleFactura create(DetalleFactura detalleFactura);
	public DetalleFactura update(Integer id, Integer idf, DetalleFactura detalleFactura);
	public void delete(Integer id,Integer idf);
	public List<DetalleFactura> findAll();
	public DetalleFactura findOne(Integer id,Integer idf);
	public List<DetalleFactura> findFactura(Integer id);
}
