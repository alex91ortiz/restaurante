package com.respage.model.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import com.respage.model.entities.Cocinero;

public interface CocineroMapper  {

	@Insert("INSERT INTO res_cocinero(nombre, apellido1, apellido2) VALUES(#{nombre}, #{apellido1}, #{apellido2})")
	@Options(useGeneratedKeys=true, keyProperty="id", keyColumn="idcocinero")
	public int insert(Cocinero cocinero);

	@Update("UPDATE res_cocinero SET nombre = #{nombre}, apellido1 = #{apellido1}, apellido2 = #{apellido2} WHERE idcocinero=#{id}")
	public int update(Cocinero cocinero);

	@Delete("DELETE FROM res_cocinero WHERE idcocinero=#{id}")
	public int deleteById(Integer id);

	@Select("SELECT c.idcocinero, c.nombre, c.apellido1, c.apellido2 FROM res_cocinero c")
	@Results(value = {
			  @Result(property = "id", column = "idcocinero"),
			  @Result(property = "nombre", column = "nombre"),
			  @Result(property = "apellido1", column = "apellido1"),
			  @Result(property = "apellido2", column = "apellido2")
			})
	public List<Cocinero> getAll();

	@Select("SELECT c.idcocinero, c.nombre, c.apellido1, c.apellido2 FROM res_cocinero c WHERE c.idcocinero = #{id}")
	@Results(value = {
			  @Result(property = "id", column = "idcocinero"),
			  @Result(property = "nombre", column = "nombre"),
			  @Result(property = "apellido1", column = "apellido1"),
			  @Result(property = "apellido2", column = "apellido2")
			})
	public Cocinero getById(@Param("id") Integer id);
}
