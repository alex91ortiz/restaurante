package com.respage.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.UriComponentsBuilder;

import com.respage.model.entities.Cliente;
import com.respage.model.services.ClienteService;


@Controller
@RequestMapping("cliente")
public class ClienteController {
	@Autowired
	ClienteService clienteService; 
	@GetMapping("cliente/{id}")
	public ResponseEntity<Cliente> getClienteId(@PathVariable("id") Integer id){
		Cliente cliente = clienteService.findOne(id);
		return new ResponseEntity<Cliente> (cliente, HttpStatus.OK);
	}
	@GetMapping("cliente")
	public ResponseEntity<List<Cliente>> getAllCliente(){
		final List<Cliente> listcliente = new ArrayList<Cliente>();
		clienteService.findAll().forEach(new Consumer<Cliente>() {
			@Override
			public void accept(Cliente e) {
				listcliente.add(e);
			}
		});
		return new ResponseEntity<List<Cliente>>(listcliente,HttpStatus.OK);
	}
	@PostMapping("cliente")
	public ResponseEntity<Cliente> addCliente(@RequestBody Cliente cliente, UriComponentsBuilder builder){
		Cliente flag = clienteService.create(cliente);
		if(flag==null) {
			return new ResponseEntity<Cliente>(flag,HttpStatus.CONFLICT);	
		}
		return new ResponseEntity<Cliente>(flag,HttpStatus.OK);
	}
	
	@PutMapping("cliente/{id}")
	public ResponseEntity<Cliente> updateCliente(@RequestBody Cliente cliente,@PathVariable("id") Integer id){
		Cliente flag = clienteService.update(id, cliente);
		if(flag==null) {
			return new ResponseEntity<Cliente>(flag,HttpStatus.CONFLICT);	
		}
		return new ResponseEntity<Cliente>(flag,HttpStatus.OK);
	}
	@DeleteMapping("cliente/{id}")
	public ResponseEntity<Void> deleteCliente(@PathVariable("id") Integer id){
		clienteService.delete(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
}
