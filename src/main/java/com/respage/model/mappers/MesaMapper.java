package com.respage.model.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import com.respage.model.entities.Mesa;

public interface MesaMapper  {

	@Insert("INSERT INTO res_mesa(numeromaxcomensales, ubicacion) VALUES(#{numeromaxcomensales}, #{ubicacion})")
	@Options(useGeneratedKeys=true, keyProperty="id", keyColumn="idmesa")
	public int insert(Mesa mesa);

	@Update("UPDATE res_mesa SET numeromaxcomensales = #{numeromaxcomensales}, ubicacion = #{ubicacion} WHERE idmesa=#{id}")
	public int update(Mesa mesa);

	@Delete("DELETE FROM res_mesa WHERE idmesa=#{id}")
	public int deleteById(Integer id);

	@Select("SELECT c.idmesa, c.numeromaxcomensales, c.ubicacion FROM res_mesa c")
	@Results(value = {
			  @Result(property = "id", column = "idmesa"),
			  @Result(property = "numeromaxcomensales", column = "numeromaxcomensales"),
			  @Result(property = "ubicacion", column = "ubicacion")
              
			})
	public List<Mesa> getAll();

	@Select("SELECT c.idmesa, c.numeromaxcomensales, c.ubicacion FROM res_mesa c WHERE c.idmesa = #{id}")
	@Results(value = {
			  @Result(property = "id", column = "idmesa"),
			  @Result(property = "numeromaxcomensales", column = "numeromaxcomensales"),
			  @Result(property = "ubicacion", column = "ubicacion")
			})
	public Mesa getById(@Param("id") Integer id);
}
