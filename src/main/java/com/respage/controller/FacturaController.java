package com.respage.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.UriComponentsBuilder;

import com.respage.model.entities.Cliente;
import com.respage.model.entities.DetalleFactura;
import com.respage.model.entities.Factura;
import com.respage.model.services.ClienteService;
import com.respage.model.services.DetalleFacturaService;
import com.respage.model.services.FacturaService;

@Controller
@RequestMapping("factura")
public class FacturaController {
	@Autowired
	FacturaService facturaService;
	@Autowired
	ClienteService clienteService;
	@Autowired
	DetalleFacturaService detalleFacturaService; 
	@GetMapping("factura/{id}")
	public ResponseEntity<Factura> getFacturaId(@PathVariable("id") Integer id){
		Factura factura = facturaService.findOne(id);
		return new ResponseEntity<Factura> (factura, HttpStatus.OK);
	}
	@GetMapping("factura")
	public ResponseEntity<List<Factura>> getAllFactura(){
		final List<Factura> listfactura = new ArrayList<Factura>();
		facturaService.findAll().forEach(new Consumer<Factura>() {
			@Override
			public void accept(Factura e) {
				listfactura.add(e);
			}
		});
		return new ResponseEntity<List<Factura>>(listfactura,HttpStatus.OK);
	}
	@PostMapping("factura")
	public ResponseEntity<Factura> addFactura(@RequestBody Factura factura, UriComponentsBuilder builder){
		Cliente cliente = factura.getCliente();
		if(cliente!=null) {
			clienteService.create(cliente);
			factura.setIdcliente(cliente.getId());
		}
		
		Factura flag = facturaService.create(factura);
		
		if(flag==null) {
			return new ResponseEntity<Factura>(flag,HttpStatus.CONFLICT);	
		}
		for(DetalleFactura objdf : flag.getDetallefactura()) {
			objdf.setFactura(flag.getId());
			detalleFacturaService.create(objdf);
		}
		return new ResponseEntity<Factura>(flag,HttpStatus.OK);
	}
	
	@PutMapping("factura/{id}")
	public ResponseEntity<Factura> updateFactura(@RequestBody Factura factura,@PathVariable("id") Integer id){
		Factura flag = facturaService.update(id, factura);
		if(flag==null) {
			return new ResponseEntity<Factura>(flag,HttpStatus.CONFLICT);	
		}
		return new ResponseEntity<Factura>(flag,HttpStatus.OK);
	}
	@DeleteMapping("factura/{id}")
	public ResponseEntity<Void> deleteFactura(@PathVariable("id") Integer id){
		facturaService.delete(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
}
