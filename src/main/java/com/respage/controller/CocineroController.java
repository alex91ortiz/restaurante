package com.respage.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.UriComponentsBuilder;

import com.respage.model.entities.Cocinero;
import com.respage.model.services.CocineroService;

@Controller
@RequestMapping("cocinero")
public class CocineroController {
	@Autowired
	CocineroService cocineroService; 
	@GetMapping("cocinero/{id}")
	public ResponseEntity<Cocinero> getCocineroId(@PathVariable("id") Integer id){
		Cocinero cocinero = cocineroService.findOne(id);
		return new ResponseEntity<Cocinero> (cocinero, HttpStatus.OK);
	}
	@GetMapping("cocinero")
	public ResponseEntity<List<Cocinero>> getAllCocinero(){
		final List<Cocinero> listcocinero = new ArrayList<Cocinero>();
		cocineroService.findAll().forEach(new Consumer<Cocinero>() {
			@Override
			public void accept(Cocinero e) {
				listcocinero.add(e);
			}
		});
		return new ResponseEntity<List<Cocinero>>(listcocinero,HttpStatus.OK);
	}
	@PostMapping("cocinero")
	public ResponseEntity<Cocinero> addCocinero(@RequestBody Cocinero cocinero, UriComponentsBuilder builder){
		Cocinero flag = cocineroService.create(cocinero);
		if(flag==null) {
			return new ResponseEntity<Cocinero>(flag,HttpStatus.CONFLICT);	
		}
		return new ResponseEntity<Cocinero>(flag,HttpStatus.OK);
	}
	
	@PutMapping("cocinero/{id}")
	public ResponseEntity<Cocinero> updateCocinero(@RequestBody Cocinero cocinero,@PathVariable("id") Integer id){
		Cocinero flag = cocineroService.update(id, cocinero);
		if(flag==null) {
			return new ResponseEntity<Cocinero>(flag,HttpStatus.CONFLICT);	
		}
		return new ResponseEntity<Cocinero>(flag,HttpStatus.OK);
	}
	@DeleteMapping("cocinero/{id}")
	public ResponseEntity<Void> deleteCocinero(@PathVariable("id") Integer id){
		cocineroService.delete(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
}