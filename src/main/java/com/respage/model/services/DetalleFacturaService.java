package com.respage.model.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.respage.model.entities.DetalleFactura;
import com.respage.model.mappers.DetalleFacturaMapper;
@Service
public class DetalleFacturaService implements IDetalleFacturaService {

	private DetalleFacturaMapper detalleFacturaMapper;

	public DetalleFacturaService(final DetalleFacturaMapper detalleFacturaMapper) {
		this.detalleFacturaMapper = detalleFacturaMapper;
	}

	@Override
	public DetalleFactura create(DetalleFactura detalleFactura) {
		this.detalleFacturaMapper.insert(detalleFactura);
		return detalleFactura;
	}

	@Override
	public DetalleFactura update(Integer id,Integer idf, DetalleFactura detalleFactura) {
		DetalleFactura detalleFacturaBD = this.detalleFacturaMapper.getById(id,idf);
		if (detalleFacturaBD == null) {
			return null;
		}
		detalleFacturaBD.setFactura(detalleFactura.getId());
		detalleFacturaBD.setCocinero(detalleFactura.getCocinero());
		detalleFacturaBD.setPlato(detalleFactura.getPlato());
        detalleFacturaBD.setImporte(detalleFactura.getImporte());
		

		this.detalleFacturaMapper.update(detalleFacturaBD);
		return detalleFacturaBD;
	}

	@Override
	public void delete(Integer id,Integer idf)  {
		if (this.detalleFacturaMapper.getById(id,idf) != null) {
			this.detalleFacturaMapper.deleteById(id,idf);
		}
	}

	@Override
	public List<DetalleFactura> findAll() {
		return this.detalleFacturaMapper.getAll();
	}

	@Override
	public DetalleFactura findOne(Integer id,Integer idf) {
		DetalleFactura detalleFactura = this.detalleFacturaMapper.getById(id,idf);
		if (detalleFactura == null) {
			return null;
		}
		return detalleFactura;
	}

	@Override
	public List<DetalleFactura> findFactura(Integer id) {
		// TODO Auto-generated method stub
		return this.detalleFacturaMapper.getByFactura(id);
	}
}
