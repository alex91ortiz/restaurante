package com.respage.model.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import com.respage.model.entities.Cliente;

public interface ClienteMapper  {

	@Insert("INSERT INTO res_cliente(nombre, apellido1, apellido2, observacion) VALUES(#{nombre}, #{apellido1}, #{apellido2}, #{observacion})")
	@Options(useGeneratedKeys=true, keyProperty="id", keyColumn="idcliente")
	public int insert(Cliente cliente);

	@Update("UPDATE res_cliente SET nombre = #{nombre}, apellido1 = #{apellido1}, apellido2 = #{apellido2}, observacion=#{observacion} WHERE idcliente=#{id}")
	public int update(Cliente cliente);

	@Delete("DELETE FROM res_cliente WHERE idcliente=#{id}")
	public int deleteById(Integer id);

	@Select("SELECT c.idcliente, c.nombre, c.apellido1, c.apellido2, c.observacion FROM res_cliente c")
	@Results(value = {
			  @Result(property = "id", column = "idcliente"),
			  @Result(property = "nombre", column = "nombre"),
			  @Result(property = "apellido1", column = "apellido1"),
			  @Result(property = "apellido2", column = "apellido2"),
              @Result(property = "observacion", column = "observacion")
              
			})
	public List<Cliente> getAll();

	@Select("SELECT c.idcliente, c.nombre, c.apellido1, c.apellido2, c.observacion FROM res_cliente c WHERE c.idcliente = #{id}")
	@Results(value = {
			  @Result(property = "id", column = "idcliente"),
			  @Result(property = "nombre", column = "nombre"),
			  @Result(property = "apellido1", column = "apellido1"),
			  @Result(property = "apellido2", column = "apellido2"),
              @Result(property = "observacion", column = "observacion")
			})
	public Cliente getById(@Param("id") Integer id);
	
	
}
