package com.respage.model.services;

import java.util.List;

import com.respage.model.entities.Cliente;

public interface IClienteService  {

	public Cliente create(Cliente cliente);
	public Cliente update(Integer id, Cliente cliente);
	public void delete(Integer id);
	public List<Cliente> findAll();
	public Cliente findOne(Integer id);
}
