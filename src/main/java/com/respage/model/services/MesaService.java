package com.respage.model.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.respage.model.entities.Mesa;
import com.respage.model.mappers.MesaMapper;
@Service
public class MesaService implements IMesaService {

	private MesaMapper mesaMapper;

	public MesaService(final MesaMapper mesaMapper) {
		this.mesaMapper = mesaMapper;
	}

	@Override
	public Mesa create(Mesa mesa) {
		this.mesaMapper.insert(mesa);
		return mesa;
	}

	@Override
	public Mesa update(Integer id, Mesa mesa) {
		Mesa mesaBD = this.mesaMapper.getById(id);
		if (mesaBD == null) {
			return null;
		}
		mesaBD.setNumeromaxcomensales(mesa.getNumeromaxcomensales());
		mesaBD.setUbicacion(mesa.getUbicacion());
		

		this.mesaMapper.update(mesaBD);
		return mesaBD;
	}

	@Override
	public void delete(Integer id)  {
		if (this.mesaMapper.getById(id) != null) {
			this.mesaMapper.deleteById(id);
		}
	}

	@Override
	public List<Mesa> findAll() {
		return this.mesaMapper.getAll();
	}

	@Override
	public Mesa findOne(Integer id) {
		Mesa mesa = this.mesaMapper.getById(id);
		if (mesa == null) {
			return null;
		}
		return mesa;
	}
}
