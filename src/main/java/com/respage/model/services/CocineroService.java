package com.respage.model.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.respage.model.entities.Cocinero;
import com.respage.model.mappers.CocineroMapper;
@Service
public class CocineroService implements ICocineroService{

	private CocineroMapper cocineroMapper;

	public CocineroService(final CocineroMapper cocineroMapper) {
		this.cocineroMapper = cocineroMapper;
	}

	@Override
	public Cocinero create(Cocinero cocinero) {
		this.cocineroMapper.insert(cocinero);
		return cocinero;
	}

	@Override
	public Cocinero update(Integer id, Cocinero cocinero) {
		Cocinero cocineroBD = this.cocineroMapper.getById(id);
		if (cocineroBD == null) {
			return null;
		}
		cocineroBD.setNombre(cocinero.getNombre());
		cocineroBD.setApellido1(cocinero.getApellido1());
		cocineroBD.setApellido2(cocinero.getApellido2());
		

		this.cocineroMapper.update(cocineroBD);
		return cocineroBD;
	}

	@Override
	public void delete(Integer id)  {
		if (this.cocineroMapper.getById(id) != null) {
			this.cocineroMapper.deleteById(id);
		}
	}

	@Override
	public List<Cocinero> findAll() {
		return this.cocineroMapper.getAll();
	}

	@Override
	public Cocinero findOne(Integer id) {
		Cocinero cocinero = this.cocineroMapper.getById(id);
		if (cocinero == null) {
			return null;
		}
		return cocinero;
	}
}
