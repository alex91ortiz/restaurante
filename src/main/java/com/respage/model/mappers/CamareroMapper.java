package com.respage.model.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import com.respage.model.entities.Camarero;

public interface CamareroMapper {

	@Insert("INSERT INTO res_camarero(nombre, apellido1, apellido2) VALUES(#{nombre}, #{apellido1}, #{apellido2})")
	@Options(useGeneratedKeys=true, keyProperty="id", keyColumn="idcamarero")
	public int insert(Camarero camarero);

	@Update("UPDATE res_camarero SET nombre = #{nombre}, apellido1 = #{apellido1}, apellido2 = #{apellido2} WHERE idcamarero=#{id}")
	public int update(Camarero camarero);

	@Delete("DELETE FROM res_camarero WHERE idcamarero=#{id}")
	public int deleteById(Integer id);

	@Select("SELECT c.idcamarero, c.nombre, c.apellido1, c.apellido2 FROM res_camarero c")
	@Results(value = {
			  @Result(property = "id", column = "idcamarero"),
			  @Result(property = "nombre", column = "nombre"),
			  @Result(property = "apellido1", column = "apellido1"),
			  @Result(property = "apellido2", column = "apellido2")
			})
	public List<Camarero> getAll();

	@Select("SELECT c.idcamarero, c.nombre, c.apellido1, c.apellido2 FROM res_camarero c WHERE c.idcamarero = #{id}")
	@Results(value = {
			  @Result(property = "id", column = "idcamarero"),
			  @Result(property = "nombre", column = "nombre"),
			  @Result(property = "apellido1", column = "apellido1"),
			  @Result(property = "apellido2", column = "apellido2")
			})
	public Camarero getById(@Param("id") Integer id);
}
