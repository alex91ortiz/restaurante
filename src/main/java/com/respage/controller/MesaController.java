package com.respage.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.UriComponentsBuilder;

import com.respage.model.entities.Mesa;
import com.respage.model.services.MesaService;

@Controller
@RequestMapping("mesa")
public class MesaController {
	@Autowired
	MesaService mesaService; 
	@GetMapping("mesa/{id}")
	public ResponseEntity<Mesa> getMesaId(@PathVariable("id") Integer id){
		Mesa mesa = mesaService.findOne(id);
		return new ResponseEntity<Mesa> (mesa, HttpStatus.OK);
	}
	@GetMapping("mesa")
	public ResponseEntity<List<Mesa>> getAllMesa(){
		final List<Mesa> listmesa = new ArrayList<Mesa>();
		mesaService.findAll().forEach(new Consumer<Mesa>() {
			@Override
			public void accept(Mesa e) {
				listmesa.add(e);
			}
		});
		return new ResponseEntity<List<Mesa>>(listmesa,HttpStatus.OK);
	}
	@PostMapping("mesa")
	public ResponseEntity<Mesa> addMesa(@RequestBody Mesa mesa, UriComponentsBuilder builder){
		Mesa flag = mesaService.create(mesa);
		if(flag==null) {
			return new ResponseEntity<Mesa>(flag,HttpStatus.CONFLICT);	
		}
		return new ResponseEntity<Mesa>(flag,HttpStatus.OK);
	}
	
	@PutMapping("mesa/{id}")
	public ResponseEntity<Mesa> updateMesa(@RequestBody Mesa mesa,@PathVariable("id") Integer id){
		Mesa flag = mesaService.update(id, mesa);
		if(flag==null) {
			return new ResponseEntity<Mesa>(flag,HttpStatus.CONFLICT);	
		}
		return new ResponseEntity<Mesa>(flag,HttpStatus.OK);
	}
	@DeleteMapping("mesa/{id}")
	public ResponseEntity<Void> deleteMesa(@PathVariable("id") Integer id){
		mesaService.delete(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
}