package com.respage.model.entities;

import java.io.Serializable;


public class Mesa implements Serializable{
	private int id;
	private int numeromaxcomensales;
	private int ubicacion;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getNumeromaxcomensales() {
		return numeromaxcomensales;
	}
	public void setNumeromaxcomensales(int numeromaxcomensales) {
		this.numeromaxcomensales = numeromaxcomensales;
	}
	public int getUbicacion() {
		return ubicacion;
	}
	public void setUbicacion(int ubicacion) {
		this.ubicacion = ubicacion;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + numeromaxcomensales;
		result = prime * result + ubicacion;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mesa other = (Mesa) obj;
		if (id != other.id)
			return false;
		if (numeromaxcomensales != other.numeromaxcomensales)
			return false;
		if (ubicacion != other.ubicacion)
			return false;
		return true;
	}
	
	

}
