package com.respage.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.respage.model.entities.Cliente;
import com.respage.model.mappers.ClienteMapper;
@Service
public class ClienteService implements IClienteService{
	
	
	private ClienteMapper clienteMapper;

	public ClienteService(final ClienteMapper clienteMapper) {
		this.clienteMapper = clienteMapper;
	}

	@Override
	public Cliente create(Cliente cliente) {
		this.clienteMapper.insert(cliente);
		return cliente;
	}

	@Override
	public Cliente update(Integer id, Cliente cliente) {
		Cliente clienteBD = this.clienteMapper.getById(id);
		if (clienteBD == null) {
			return null;
		}
		clienteBD.setNombre(cliente.getNombre());
		clienteBD.setApellido1(cliente.getApellido1());
		clienteBD.setApellido2(cliente.getApellido2());
        clienteBD.setObservacion(cliente.getObservacion());
		

		this.clienteMapper.update(clienteBD);
		return clienteBD;
	}

	@Override
	public void delete(Integer id)  {
		if (this.clienteMapper.getById(id) != null) {
			this.clienteMapper.deleteById(id);
		}
	}

	@Override
	public List<Cliente> findAll() {
		return this.clienteMapper.getAll();
	}

	@Override
	public Cliente findOne(Integer id) {
		Cliente cliente = this.clienteMapper.getById(id);
		if (cliente == null) {
			return null;
		}
		return cliente;
	}
}
