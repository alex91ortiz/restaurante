package com.respage.model.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.respage.model.entities.Factura;
import com.respage.model.mappers.FacturaMapper;
@Service
public class FacturaService implements IFacturaService {

	
	private FacturaMapper facturaMapper;

	public FacturaService(final FacturaMapper facturaMapper) {
		this.facturaMapper = facturaMapper;
	}

	@Override
	public Factura create(Factura factura) {
		this.facturaMapper.insert(factura);
		return factura;
	}

	@Override
	public Factura update(Integer id, Factura factura) {
		Factura facturaBD = this.facturaMapper.getById(id);
		if (facturaBD == null) {
			return null;
		}
		facturaBD.setCamarero(factura.getCamarero());
		facturaBD.setCliente(factura.getCliente());
		facturaBD.setMesa(factura.getMesa());
        facturaBD.setFechaFactura(factura.getFechaFactura());
		

		this.facturaMapper.update(facturaBD);
		return facturaBD;
	}

	@Override
	public void delete(Integer id)  {
		if (this.facturaMapper.getById(id) != null) {
			this.facturaMapper.deleteById(id);
		}
	}

	@Override
	public List<Factura> findAll() {
		return this.facturaMapper.getAll();
	}

	@Override
	public Factura findOne(Integer id) {
		Factura factura = this.facturaMapper.getById(id);
		if (factura == null) {
			return null;
		}
		return factura;
	}
}
