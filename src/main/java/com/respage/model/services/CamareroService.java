package com.respage.model.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.respage.model.entities.Camarero;
import com.respage.model.mappers.CamareroMapper;
@Service
public class CamareroService implements ICamareroService{

	private CamareroMapper camareroMapper;

	public CamareroService(final CamareroMapper camareroMapper) {
		this.camareroMapper = camareroMapper;
	}

	@Override
	public Camarero create(Camarero camarero) {
		this.camareroMapper.insert(camarero);
		return camarero;
	}

	@Override
	public Camarero update(Integer id, Camarero camarero) {
		Camarero camareroBD = this.camareroMapper.getById(id);
		if (camareroBD == null) {
			return null;
		}
		camareroBD.setNombre(camarero.getNombre());
		camareroBD.setApellido1(camarero.getApellido1());
		camareroBD.setApellido2(camarero.getApellido2());
		

		this.camareroMapper.update(camareroBD);
		return camareroBD;
	}

	@Override
	public void delete(Integer id)  {
		if (this.camareroMapper.getById(id) != null) {
			this.camareroMapper.deleteById(id);
		}
	}

	@Override
	public List<Camarero> findAll() {
		return this.camareroMapper.getAll();
	}

	@Override
	public Camarero findOne(Integer id) {
		Camarero camarero = this.camareroMapper.getById(id);
		if (camarero == null) {
			return null;
		}
		return camarero;
	}
}
