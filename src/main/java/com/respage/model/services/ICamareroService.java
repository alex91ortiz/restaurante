package com.respage.model.services;

import java.util.List;


import com.respage.model.entities.Camarero;

public interface ICamareroService {


	public Camarero create(Camarero camarero);
	public Camarero update(Integer id, Camarero camarero);
	public void delete(Integer id);
	public List<Camarero> findAll();
	public Camarero findOne(Integer id);
}
