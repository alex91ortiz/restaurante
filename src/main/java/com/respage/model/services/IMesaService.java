package com.respage.model.services;

import java.util.List;

import com.respage.model.entities.Mesa;

public interface IMesaService  {

	public Mesa create(Mesa mesa);
	public Mesa update(Integer id, Mesa mesa);
	public void delete(Integer id);
	public List<Mesa> findAll();
	public Mesa findOne(Integer id);
}
