package com.respage.model.services;

import java.util.List;

import com.respage.model.entities.Factura;

public interface IFacturaService  {

	public Factura create(Factura factura);
	public Factura update(Integer id, Factura factura);
	public void delete(Integer id);
	public List<Factura> findAll();
	public Factura findOne(Integer id);
}
