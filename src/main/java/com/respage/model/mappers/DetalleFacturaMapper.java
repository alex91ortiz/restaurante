package com.respage.model.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import com.respage.model.entities.DetalleFactura;

public interface DetalleFacturaMapper  {

	@Insert("INSERT INTO res_detallefactura(idfactura, idcocinero, plato, importe) VALUES(#{factura}, #{cocinero}, #{plato}, #{importe})")
	@Options(useGeneratedKeys=true, keyProperty="id", keyColumn="iddetallefactura")
	public int insert(DetalleFactura factura);

	@Update("UPDATE res_detallefactura SET  idcocinero = #{IdCocinero}, plato = #{plato}, importe=#{importe} WHERE iddetallefactura=#{id}")
	public int update(DetalleFactura factura);

	@Delete("DELETE FROM res_detallefactura WHERE idfactura=#{factura} AND iddetallefactura=#{id}")
	public int deleteById(Integer id,Integer idf);

	@Select("SELECT c.iddetallefactura, c.idfactura, c.idcocinero, c.plato, c.importe FROM res_detallefactura c")
	@Results(value = {
			  @Result(property = "id", column = "iddetallefactura"),
			  @Result(property = "factura", column = "idfactura"),
			  @Result(property = "cocinero", column = "IdCocinero"),
			  @Result(property = "plato", column = "plato"),
              @Result(property = "importe", column = "importe")
              
			})
	public List<DetalleFactura> getAll();

	@Select("SELECT c.iddetallefactura, c.idfactura, c.idcocinero, c.plato, c.importe  FROM res_detallefactura c WHERE iddetallefactura=#{id} AND idfactura=#{factura}")
	@Results(value = {
			  @Result(property = "id", column = "iddetallefactura"),
			  @Result(property = "factura", column = "idfactura"),
			  @Result(property = "cocinero", column = "IdCocinero"),
			  @Result(property = "plato", column = "plato"),
            @Result(property = "importe", column = "importe")
			})
	public DetalleFactura getById(@Param("id") Integer id,@Param("factura") Integer idf);
	
	@Select("SELECT c.iddetallefactura, c.idfactura, c.idcocinero, c.plato, c.importe  FROM res_detallefactura c WHERE  idfactura=#{factura}")
	@Results(value = {
			  @Result(property = "IdDetalleFactura", column = "iddetallefactura"),
			  @Result(property = "IdFactura", column = "idfactura"),
			  @Result(property = "IdCocinero", column = "idcocinero"),
			  @Result(property = "plato", column = "plato"),
              @Result(property = "importe", column = "importe")
			})
	public List<DetalleFactura> getByFactura(@Param("id") Integer id);
}
