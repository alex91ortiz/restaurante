package com.respage.model.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;



public class Factura implements Serializable{
	
	private int id;
	private int camarero;
	private Cliente cliente;
	private int idcliente;
	private int mesa;
	private Date fechaFactura;
	private List<DetalleFactura> detallefactura;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCamarero() {
		return camarero;
	}
	public void setCamarero(int camarero) {
		this.camarero = camarero;
	}

	public int getMesa() {
		return mesa;
	}
	public void setMesa(int mesa) {
		this.mesa = mesa;
	}
	public Date getFechaFactura() {
		return fechaFactura;
	}
	public void setFechaFactura(Date fechaFactura) {
		this.fechaFactura = fechaFactura;
	}
	public List<DetalleFactura> getDetallefactura() {
		return detallefactura;
	}
	public void setDetallefactura(List<DetalleFactura> detallefactura) {
		this.detallefactura = detallefactura;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	
	public int getIdcliente() {
		return idcliente;
	}
	public void setIdcliente(int idcliente) {
		this.idcliente = idcliente;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + camarero;
		result = prime * result + ((cliente == null) ? 0 : cliente.hashCode());
		result = prime * result + ((detallefactura == null) ? 0 : detallefactura.hashCode());
		result = prime * result + ((fechaFactura == null) ? 0 : fechaFactura.hashCode());
		result = prime * result + id;
		result = prime * result + idcliente;
		result = prime * result + mesa;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Factura other = (Factura) obj;
		if (camarero != other.camarero)
			return false;
		if (cliente == null) {
			if (other.cliente != null)
				return false;
		} else if (!cliente.equals(other.cliente))
			return false;
		if (detallefactura == null) {
			if (other.detallefactura != null)
				return false;
		} else if (!detallefactura.equals(other.detallefactura))
			return false;
		if (fechaFactura == null) {
			if (other.fechaFactura != null)
				return false;
		} else if (!fechaFactura.equals(other.fechaFactura))
			return false;
		if (id != other.id)
			return false;
		if (idcliente != other.idcliente)
			return false;
		if (mesa != other.mesa)
			return false;
		return true;
	}
	
	
	
}
