package com.respage.model.services;

import java.util.List;

import com.respage.model.entities.Cocinero;

public interface ICocineroService  {

	public Cocinero create(Cocinero cocinero);
	public Cocinero update(Integer id, Cocinero cocinero);
	public void delete(Integer id);
	public List<Cocinero> findAll();
	public Cocinero findOne(Integer id);
}
