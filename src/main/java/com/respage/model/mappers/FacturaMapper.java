package com.respage.model.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import com.respage.model.entities.Factura;

public interface FacturaMapper  {

	@Insert("INSERT INTO res_factura(idcliente, idcamarero, idmesa, fechafactura) VALUES(#{idcliente}, #{camarero}, #{mesa}, #{fechaFactura})")
	@Options(useGeneratedKeys=true, keyProperty="id", keyColumn="idfactura")
	public int insert(Factura factura);

	@Update("UPDATE res_factura SET idcliente = #{idcliente}, idcamarero = #{camarero}, idmesa = #{mesa}, fechafactura=#{fechaFactura} WHERE idfactura=#{id}")
	public int update(Factura factura);

	@Delete("DELETE FROM res_factura WHERE idfactura=#{id}")
	public int deleteById(Integer id);

	@Select("SELECT c.idfactura, c.idcliente, c.idcamarero, c.idmesa, c.fechafactura FROM res_factura c")
	@Results(value = {
			  @Result(property = "id", column = "idfactura"),
			  @Result(property = "idcliente", column = "idcliente"),
			  @Result(property = "camarero", column = "idcamarero"),
			  @Result(property = "mesa", column = "idmesa"),
              @Result(property = "fechaFactura", column = "fechafactura")
              
			})
	public List<Factura> getAll();

	@Select("SELECT c.idfactura, c.idcliente, c.idcamarero, c.idmesa, c.fechafactura FROM res_factura c WHERE c.idfactura = #{id}")
	@Results(value = {
			  @Result(property = "id", column = "idfactura"),
			  @Result(property = "idcliente", column = "idcliente"),
			  @Result(property = "camarero", column = "idcamarero"),
			  @Result(property = "mesa", column = "idmesa"),
              @Result(property = "fechaFactura", column = "fechafactura")
			})
	public Factura getById(@Param("id") Integer id);
}
